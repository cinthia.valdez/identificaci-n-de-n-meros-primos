# Autora : Cinthia Valdez
# Email : cinthia.valdez@unl.edu.ec
# verificar si un numero entero es primo o no

def es_primo(valor):
    if valor < 2:
        return "No es un número primo"
    for n in range(2, valor):
        if valor % n == 0:
            return "No es un número primo"
    return "Si es un número primo"
print("Ingerese un valor")
valor = int(input())
print(es_primo(valor))